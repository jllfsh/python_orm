import psycopg2
from entity import Entity


class Article(Entity):
    _fields = ['title', 'text']


class Category(Entity):
    _fields = ['title']


class Tag(Entity):
    _fields = ['value']

Entity.db = psycopg2.connect(database='articles', user='admin', password='admin', host='localhost')

# SELECT
# a = Article(1)
# print(a.id, a.title, a.text, a.created, a.updated)
# a.goo = 4
# print(a.goo)

# UPDATE
# a = Article(7)
# a.text = 'Simple text'
# a.save()
# print(a.text)
#
# a = Article(8)
# a.text = 'Very interesting content with some freakin\' "quotes"'
# a.title = 'owla'
# a.save()
# print(a.id, a.title, a.text)
#
# a = Article(8)
# a.text = 'Very interesting content with some freakin\' "quotes"'
# print(a.id, a.title, a.text)

# INSERT
# a = Article()
# a.title = 'New title'
# a.save()
# print(a.title)
#
# a = Article()
# a.title = 'New interesting title'
# a.text = 'Some interesting text'
# a.save()
# print(a.id, a.title, a.text)
#
# a = Article()
# a.title = 'Third title'
# a.text = 'Very interesting content with some freakin\' "quotes"'
# a.save()
# print(a.id, a.title, a.text)
#
# a.title = 'Bugs are wonderful'
# a.save()
# print(a.id, a.title, a.text)

# DELETE
# a = Article(17)
# a.delete()
#
# a = Article(170)
# a.delete()
#
# a = Article()
# a.delete()

# GET ALL
# for a in Article.all():
#     print a.id, a.title

# CATEGORY
# INSERT
# c = Category()
# c.title = 'New title'
# c.save()
# print(c.title)

# TAG
# INSERT
# t = Tag()
# t.value = 'New value'
# t.save()
# print(t.value)
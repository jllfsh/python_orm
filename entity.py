import datetime
import psycopg2
import psycopg2.extras


class DatabaseError(Exception):
    pass


class OrmException(Exception):
    pass


class NotFoundError(Exception):
    pass


class Entity(object):
    """Basic ORM class"""

    # Connection to database
    db = None

    # Sample queries
    __select_query = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __list_query = 'SELECT * FROM "{table}"'
    __insert_query = 'INSERT INTO "{table}" ({columns}) VALUES ({placeholders}) RETURNING "{table}_id"'
    __update_query = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'
    __delete_query = 'DELETE FROM "{table}" WHERE {table}_id=%s'

    def __init__(self, id_=None):
        """Class init"""

        # Check database connection
        if not self.__class__.db:
            raise DatabaseError('Connection error')

        # Initialize cursor
        self.__cursor = self.__class__.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        # Dict with attributes and values
        self.__fields = {}
        # Set ID
        self.__id = id_
        # Set attributes loaded from database to model flag
        self.__loaded = False
        # Set attributes in model was modified flag
        self.__modified = False
        # Set table name
        self.__table = self.__class__.__name__.lower()

        # Load attributes from database to model if ID parameter was set
        if self.__id:
            self.__load()

    def __getattr__(self, name):
        """Getter"""

        # Check, if instance is modified and throw an exception
        if self.__modified:
            raise OrmException('Instance is modified')

        # Check, if requested property name is in current class and throw an exception
        if name not in self._fields:
            raise AttributeError('Unrecognized attribute')

        # Get corresponding data from database if needed
        if not self.__loaded:
            self.__load()

        # Call corresponding getter with name as an argument
        return self._get_column(name)

    def __setattr__(self, name, value):
        """Setter"""

        # Check, if requested property name is in current class and call corresponding
        # setter with name and value as arguments or use default implementation
        self._set_column(name, value) if name in self._fields else super(Entity, self).__setattr__(name, value)

    def _get_column(self, name):
        """Getter for columns/fields"""

        # Return value from fields array by <table>_<name> as a key
        return self.__fields['_'.join([self.__table, name])]

    def _set_column(self, name, value):
        """Setter for columns/fields"""

        # Put new value into fields array with <table>_<name> as a key and set modified flag
        self.__fields['_'.join([self.__table, name])] = value
        self.__modified = True

    def __load(self):
        """Load attributes from database to model's fields dictionary"""

        # Get data from database
        self.__execute_query(self.__select_query.format(table=self.__table), (self.__id,))

        # Check, if item was found by ID
        if not self.__cursor.rowcount:
            raise NotFoundError('Item not found')

        # Set fields dictionary and loaded flag
        self.__fields = self.__cursor.fetchone()
        self.__loaded = True

    def __execute_query(self, query, args):
        """Execute queries with or without args"""

        # Execute an SQL statement and handle exceptions together with transactions
        try:
            self.__cursor.execute(query, args) if args else self.__cursor.execute(query)
            self.__class__.db.commit()
        except Exception as e:
            self.__class__.db.rollback()
            raise DatabaseError(e.message)

    def __insert(self):
        """Handle insert queries"""

        # Check, if any field was set
        if not self.__fields:
            raise OrmException('No attributes to insert')

        # Prepare parameters
        # Set created and updated fields
        self.__fields[self.__table + '_created'] = self.__fields[self.__table + '_updated'] = datetime.datetime.now()
        # Set table's columns name
        columns = self.__fields.keys()
        # Set placeholders
        placeholders = '%s' if len(self.__fields) == 1 else ', '.join('%s'.split() * len(self.__fields))
        # Set values
        values = self.__fields.values()

        # Execute the insert query with prepared parameters
        self.__execute_query(self.__insert_query.format(table=self.__table, columns=', '.join(columns),
                                                        placeholders=placeholders), tuple(values))

        # Set returned ID as the model's parameter
        self.__id = self.__cursor.fetchone()[self.__table + '_id']

    def __update(self):
        """Handle update queries"""

        # Check, if any field was set
        if not self.__fields:
            raise OrmException('No attributes to update')

        # Prepare parameters
        # Set updated fields
        self.__fields[self.__table + '_updated'] = datetime.datetime.now()
        # Set table's columns name
        columns = map(lambda attr: attr + '=%s', self.__fields.keys())
        # Set values and add ID parameter
        values = self.__fields.values()
        values.append(self.__id)

        # Execute the update query with prepared parameters
        self.__execute_query(self.__update_query.format(table=self.__table, columns=', '.join(columns)), tuple(values))

    def delete(self):
        """Handle delete queries"""

        # Check, if ID parameter is present
        if not self.__id:
            raise RuntimeError('ID parameter must be present')

        # Execute the delete query with appropriate id
        self.__execute_query(self.__delete_query.format(table=self.__table), tuple([self.__id]))

    def save(self):
        """Save changes into database"""

        # Execute either insert or update query, depending on instance ID and unset modified flag
        self.__insert() if not self.__id else self.__update()
        self.__modified = False

    @classmethod
    def all(cls):
        """Get ALL rows with ALL columns from corresponding table"""

        # Set connection
        with cls.db.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
            # Set table name
            table_name = cls.__name__.lower()

            # Execute select query
            cursor.execute(cls.__list_query.format(table=table_name))

            # If table is empty return empty list
            if not cursor.rowcount:
                return []

            # Get all items from database
            rows = cursor.fetchall()

            # Create and return list of instances
            instances = []
            for row in rows:
                # For each row create an instance of appropriate class
                instance = cls()
                # Each instance is filled with column data and correct ID
                instance.__id = row[table_name + '_id']
                instance.__fields = row
                # Add instance to list
                instances.append(instance)
            return instances

    @property
    def id(self):
        """Return ID parameter"""

        return self.__id

    @property
    def created(self):
        """Return created parameter"""

        return self.__fields[self.__table + '_created']

    @property
    def updated(self):
        """Return updated parameter"""

        return self.__fields[self.__table + '_updated']
